# Relaxed Duel

一个游戏王的计算器，主要用于辅助记录场上卡牌信息。简单的提供计算器、撤销、掷硬币、掷骰子功能。

![工具演示](https://relaxed-duel-1258865037.cos.ap-chengdu.myqcloud.com/releax-duel-1.gif)



**操作方式**

1. 单击：响应按键功能或计数+1
2. 双击：计数-1
3. 长按：清空该项数据

