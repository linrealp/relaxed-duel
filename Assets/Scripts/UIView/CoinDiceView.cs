using UnityEngine;
using EchoFramework;
using UnityEngine.UI;

/// <summary>
/// 硬币骰子选择界面
/// </summary>
public class CoinDiceView : UIViewBase
{
    private EchoButton bgBtn;
    private RectTransform mainTrans;
    private GridLayoutGroup showGridLayoutGroup;
    private EchoText titleTxt;

    /// <summary>
    /// 是否是硬币选择界面
    /// </summary>
    private bool isCoinSelectView;

    /// <summary>
    /// 阵营
    /// </summary>
    private CampEnum camp;


    public override void OnAwake()
    {
        base.OnAwake();
        bgBtn = prefabBinder.Find("BgBtn").GetComponent<EchoButton>();
        bgBtn.onClick.AddListener(OnClickBgBtn);
        mainTrans = prefabBinder.Find("MainTrans").GetComponent<RectTransform>();
        showGridLayoutGroup = prefabBinder.Find("ShowGridLayoutGroup").GetComponent<GridLayoutGroup>();
        titleTxt = prefabBinder.Find("TitleTxt").GetComponent<EchoText>();
    }

    public override void OnStart(params object[] objs)
    {
        base.OnStart(objs);
        camp = (CampEnum) objs[0];
        if (camp == CampEnum.Player2)
        {
            mainTrans.localRotation = Quaternion.Euler(180f, 180f, 0);
        }

        isCoinSelectView = ViewID == (int) UIViewID.CoinSelectView;
        titleTxt.LoadLang(isCoinSelectView ? "SelectCoin" : "SelectDice");

        for (int i = 0; i < 3; i++)
        {
            GameObject coinDiceGo = AssetManager.Instance.LoadGameObject("Assets/GameRes/UIPrefab/CoinDiceItem.prefab");
            CoinDiceItem coinDiceItem = coinDiceGo.GetComponent<CoinDiceItem>();
            CoinDiceEnum coinDiceEnum = isCoinSelectView ? CoinDiceEnum.Coin : CoinDiceEnum.Dice;
            coinDiceItem.SetData(camp, isCoinSelectView, coinDiceEnum, i + 1);
            coinDiceGo.transform.SetParent(showGridLayoutGroup.transform, false);
        }
    }

    private void OnClickBgBtn()
    {
        CloseWindow();
    }
}