using UnityEngine;
using UnityEngine.UI;
using EchoFramework;

/// <summary>
/// 硬币骰子动画界面
/// </summary>
public class CoinDiceAnimView : UIViewBase
{
    private RectTransform mainRectTrans;
    private GridLayoutGroup showGridLayouGroup;

    /// <summary>
    /// 自动关闭界面时间（单位：秒）
    /// </summary>
    private readonly float autoCloseDuringTime = 2.5f;
    
    /// <summary>
    /// 是否为硬币动画界面
    /// </summary>
    private bool isCoinAnimView;

    /// <summary>
    /// 显示数量
    /// </summary>
    private int showCnt;
    
    /// <summary>
    /// 阵营
    /// </summary>
    private CampEnum camp = CampEnum.None;
    
    public override void OnAwake()
    {
        base.OnAwake();
        showGridLayouGroup = prefabBinder.Find("ShowGridLayouGroup").GetComponent<GridLayoutGroup>();
        mainRectTrans = prefabBinder.Find("MainRectTrans").GetComponent<RectTransform>();
    }

    public override void OnStart(params object[] objs)
    {
        base.OnStart(objs);
        isCoinAnimView = (UIViewID) ViewID == UIViewID.CoinAnimView;
        camp = (CampEnum) objs[0];
        if (camp == CampEnum.Player2)
        {
            mainRectTrans.localRotation = Quaternion.Euler(180f, 180f, 0);
        }
        showCnt = (int) objs[1];
        for (int i = 0; i < showCnt; i++)
        {
            if (isCoinAnimView)
            {
                AssetManager.Instance.LoadGameObject("Assets/GameRes/UIPrefab/CoinAnimItem.prefab", showGridLayouGroup.transform);
            }
            else
            {
                AssetManager.Instance.LoadGameObject("Assets/GameRes/UIPrefab/DiceAnimItem.prefab", showGridLayouGroup.transform);
            }
        }

        TimeManager.Instance.DelaySeconed(autoCloseDuringTime, () => CloseWindow());
    }
}