using System;
using UnityEngine;
using EchoFramework;
using Random = UnityEngine.Random;

/// <summary>
/// 掷硬币动画
/// </summary>
public class CoinAnimItem : MonoBehaviour
{
    private PrefabBinder prefabBinder;
    private Animator coinAnimator;

    private void Awake()
    {
        prefabBinder = GetComponent<PrefabBinder>();
        coinAnimator = prefabBinder.Find("CoinAnimator").GetComponent<Animator>();
        
    }

    private void Start()
    {
        int rand = Random.Range(0, 2);
        int coinObverseAnimHashCode = Animator.StringToHash("CoinObverseAnim");
        int coinReverseAnimHashCode = Animator.StringToHash("CoinReverseAnim");
        coinAnimator.Play(rand == 0 ? coinObverseAnimHashCode : coinReverseAnimHashCode);
    }
}