using UnityEngine;
using EchoFramework;
using System.Collections.Generic;

/// <summary>
/// 支付生命值界面
/// </summary>
public class PayLifeView : UIViewBase
{
    private EchoButton bgBtn;
    private RectTransform mainRectTrans;

    /// <summary>
    /// 阵营
    /// </summary>
    private CampEnum camp = CampEnum.None;

    /// <summary>
    /// 生命值
    /// </summary>
    private int hpNum;

    /// <summary>
    /// 生命支付配置列表
    /// </summary>
    private List<PayLifeCfg> payLifeCfgList;
    
    public override void OnAwake()
    {
        base.OnAwake();
        bgBtn = prefabBinder.Find("BgBtn").GetComponent<EchoButton>();
        bgBtn.onClick.AddListener(OnClickBgBtn);
        mainRectTrans = prefabBinder.Find("MainRectTrans").GetComponent<RectTransform>();
    }

    public override void OnStart(params object[] objs)
    {
        base.OnStart(objs);

        if (objs != null && objs.Length >= 2)
        {
            camp = (CampEnum) objs[0];
            hpNum = (int) objs[1];
            if (camp == CampEnum.Player2)
            {
                mainRectTrans.localRotation = Quaternion.Euler(180f, 180f, 0);
            }
        }
        else
        {
            CloseWindow();
        }
        
        payLifeCfgList = ConfigManager<PayLifeCfg>.Instance.GetDataList();
        for (int i = 0; i < payLifeCfgList.Count; i++)
        {
            PayLifeCfg payLifeCfg = payLifeCfgList[i];
            GameObject payLifeGo = AssetManager.Instance.LoadGameObject("Assets/GameRes/UIPrefab/PayLifeItem.prefab");
            payLifeGo.transform.SetParent(mainRectTrans, false);
            EchoText payLifeTxt = payLifeGo.transform.Find("ShowTxt").GetComponent<EchoText>();
            EchoButton payLifeBtn = payLifeGo.transform.GetComponent<EchoButton>();
            payLifeTxt.LoadLang(payLifeCfg.Desc, payLifeCfg.CalculateNum);
            payLifeBtn.onClick.AddListener(() =>
            {
                CalculateTypeEnum calculateTypeEnum = (CalculateTypeEnum) payLifeCfg.CalculateType;
                switch (calculateTypeEnum)
                {
                    case CalculateTypeEnum.AddOperator:  //加法
                        hpNum += payLifeCfg.CalculateNum;
                        break;
                    case CalculateTypeEnum.ReduceOperator:  //减法
                        hpNum -= payLifeCfg.CalculateNum;
                        break;
                    case CalculateTypeEnum.MultiOperator:  //乘法
                        hpNum *= payLifeCfg.CalculateNum;    
                        break;
                    case CalculateTypeEnum.DivideOperator:  //除法
                        if (payLifeCfg.CalculateNum != 0)
                        {
                            float tempCaculateNum = (float) payLifeCfg.CalculateNum;
                            float floatHpNum = Mathf.Ceil(hpNum / tempCaculateNum);
                            hpNum = (int) floatHpNum;
                        }
                        break;
                }
                hpNum = hpNum < 0 ? 0 : hpNum;
                EventManager.Instance.Execute(EventTypeEnum.RefreshDisk, camp, 0, DiskEnum.HP, hpNum);
                CloseWindow();
            });            
        }
        
    }

    private void OnClickBgBtn()
    {
        CloseWindow();
    }
}