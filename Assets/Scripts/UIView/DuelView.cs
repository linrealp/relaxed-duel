using UnityEngine;
using EchoFramework;

/// <summary>
/// 决斗盘界面
/// </summary>
public class DuelView : UIViewBase
{
    private SingleDisk singleDiskPlayer1;
    private SingleDisk singleDiskPlayer2;

    /// <summary>
    /// 当前是否显示这退出游戏弹窗
    /// </summary>
    private bool isShowExitGameWindow;

    public override void OnAwake()
    {
        base.OnAwake();
        singleDiskPlayer1 = prefabBinder.Find("SingleDiskPlayer1").GetComponent<SingleDisk>();
        singleDiskPlayer2 = prefabBinder.Find("SingleDiskPlayer2").GetComponent<SingleDisk>();
    }

    public override void OnStart(params object[] objs)
    {
        base.OnStart(objs);
        singleDiskPlayer1.SetData(CampEnum.Player1);
        singleDiskPlayer2.SetData(CampEnum.Player2);
    }

    public override void OnUpdate()
    {
        base.OnUpdate();
        if (Input.GetKeyDown(KeyCode.Escape) && !isShowExitGameWindow)
        {
            isShowExitGameWindow = true;
            MessageBoxView.OpenWindow("Common_Cancel", msgBoxView => //取消
                {
                    isShowExitGameWindow = false;
                    msgBoxView.CloseWindow();
                },
                "Common_Confirm", msgBoxView => //确定
                {
#if UNITY_EDITOR
                    UnityEditor.EditorApplication.isPlaying = false;
#else
                    Application.Quit();
#endif
                }, msgBoxView =>
                {
                    isShowExitGameWindow = false;
                }, "ExitGame"); //是否要退出游戏
        }
    }
}

