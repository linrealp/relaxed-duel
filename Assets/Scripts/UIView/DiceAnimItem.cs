using System;
using UnityEngine;
using EchoFramework;
using Random = UnityEngine.Random;

/// <summary>
/// 骰子动画
/// </summary>
public class DiceAnimItem : MonoBehaviour
{
    private PrefabBinder prefabBinder;
    private EchoImage diceImg;

    /// <summary>
    /// 动画刷新时间（单位：秒）
    /// </summary>
    private readonly float animUpdateTime = 0.02f;

    /// <summary>
    /// 动画持续时间（单位：秒）
    /// </summary>
    private readonly float animDuringTime = 1f;

    /// <summary>
    /// 骰子1~6图片
    /// </summary>
    [SerializeField]
    private Sprite[] diceImgArr;

    /// <summary>
    /// 动画结束时刻（单位：秒）
    /// </summary>
    private float animEndTime;

    /// <summary>
    /// 下次刷新图片时刻（单位：秒）
    /// </summary>
    private float nextRefreshImgTime;


    private void Awake()
    {
        prefabBinder = GetComponent<PrefabBinder>();
        diceImg = prefabBinder.Find("DiceImg").GetComponent<EchoImage>();
    }

    private void Start()
    {
        animEndTime = TimeManager.TimeSeconedFloat + animDuringTime;
    }

    private void Update()
    {
        if (TimeManager.TimeSeconedFloat > animEndTime)
            return;

        if (TimeManager.TimeSeconedFloat > nextRefreshImgTime)
        {
            nextRefreshImgTime = TimeManager.TimeSeconedFloat + animUpdateTime;
            int diceImgIndex = Random.Range(1, 7);
            diceImg.sprite = diceImgArr[diceImgIndex];
        }
    }
}