using UnityEngine;
using EchoFramework;

/// <summary>
/// 骰子硬币Item
/// </summary>
public class CoinDiceItem : MonoBehaviour
{
    private PrefabBinder prefabBinder;
    private EchoButton coinDiceItemBtn;
    private RectTransform singleDiceTrans;
    private RectTransform doubleDiceTrans;
    private RectTransform tripleDiceTrans;
    private RectTransform singleCoinTrans;
    private RectTransform doubleCoinTrans;
    private RectTransform tripleCoinTrans;

    /// <summary>
    /// 是否是硬币选择界面
    /// </summary>
    private bool isCoinSelectView;

    /// <summary>
    /// 硬币骰子枚举
    /// </summary>
    private CoinDiceEnum coinDiceEnum;

    /// <summary>
    /// 阵营
    /// </summary>
    private CampEnum camp;

    /// <summary>
    /// 显示数量
    /// </summary>
    private int showCnt;

    private void Awake()
    {
        prefabBinder = GetComponent<PrefabBinder>();
        coinDiceItemBtn = prefabBinder.Find("CoinDiceItemBtn").GetComponent<EchoButton>();
        coinDiceItemBtn.onClick.AddListener(OnClickCoinDiceItemBtn);
        singleCoinTrans = prefabBinder.Find("SingleCoinTrans").GetComponent<RectTransform>();
        doubleCoinTrans = prefabBinder.Find("DoubleCoinTrans").GetComponent<RectTransform>();
        tripleCoinTrans = prefabBinder.Find("TripleCoinTrans").GetComponent<RectTransform>();
        singleDiceTrans = prefabBinder.Find("SingleDiceTrans").GetComponent<RectTransform>();
        doubleDiceTrans = prefabBinder.Find("DoubleDiceTrans").GetComponent<RectTransform>();
        tripleDiceTrans = prefabBinder.Find("TripleDiceTrans").GetComponent<RectTransform>();
    }

    /// <summary>
    /// 设置数据
    /// </summary>
    /// <param name="campEnum">阵营</param>
    /// <param name="isCoinSelectView">是否是硬币选择界面</param>
    /// <param name="coinDiceEnum">硬币骰子枚举</param>
    /// <param name="showCnt">显示数量</param>
    public void SetData(CampEnum campEnum, bool isCoinSelectView, CoinDiceEnum coinDiceEnum, int showCnt)
    {
        this.camp = campEnum;
        this.isCoinSelectView = isCoinSelectView;
        this.coinDiceEnum = coinDiceEnum;
        this.showCnt = showCnt;
        singleCoinTrans.gameObject.SetActive(this.coinDiceEnum == CoinDiceEnum.Coin && this.showCnt == 1);
        doubleCoinTrans.gameObject.SetActive(this.coinDiceEnum == CoinDiceEnum.Coin && this.showCnt == 2);
        tripleCoinTrans.gameObject.SetActive(this.coinDiceEnum == CoinDiceEnum.Coin && this.showCnt == 3);
        singleDiceTrans.gameObject.SetActive(this.coinDiceEnum == CoinDiceEnum.Dice && this.showCnt == 1);
        doubleDiceTrans.gameObject.SetActive(this.coinDiceEnum == CoinDiceEnum.Dice && this.showCnt == 2);
        tripleDiceTrans.gameObject.SetActive(this.coinDiceEnum == CoinDiceEnum.Dice && this.showCnt == 3);
    }

    private void OnClickCoinDiceItemBtn()
    {
        if (isCoinSelectView)
        {
            UIViewManager.Instance.OpenWindow(UIViewID.CoinAnimView, camp, showCnt);
            UIViewManager.Instance.CloseWindow(UIViewID.CoinSelectView);
        }
        else
        {
            UIViewManager.Instance.OpenWindow(UIViewID.DiceAnimView, camp, showCnt);
            UIViewManager.Instance.CloseWindow(UIViewID.DiceSelectView);
        }
    }
}