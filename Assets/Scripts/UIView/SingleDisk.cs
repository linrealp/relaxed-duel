using System;
using UnityEngine;
using EchoFramework;

/// <summary>
/// 单方战斗盘
/// </summary>
public class SingleDisk : MonoBehaviour
{
    private PrefabBinder prefabBinder;
    private EchoText hpTxt;
    private EchoButton divideBtn;
    private EchoButton reduceBtn;
    private EchoButton addBtn;
    private EchoButton resetBtn;
    private EchoButton undoBtn;
    private EchoButton diceBtn;
    private EchoButton coinBtn;
    private DiskItem diskItem1;
    private DiskItem diskItem2;
    private DiskItem diskItem3;
    private DiskItem diskItem4;
    private DiskItem diskItem5;
    private DiskItem diskItem6;

    /// <summary>
    /// 初始生命值
    /// </summary>
    private readonly int InitHpValue = 8000;

    /// <summary>
    /// 命令模块
    /// </summary>
    private CommandLogic commandLogic;

    /// <summary>
    /// 阵营
    /// </summary>
    private CampEnum camp = CampEnum.None;

    /// <summary>
    /// 生命值
    /// </summary>
    private int hpValue;

    private void Awake()
    {
        prefabBinder = GetComponent<PrefabBinder>();
        hpTxt = prefabBinder.Find("HPTxt").GetComponent<EchoText>();
        divideBtn = prefabBinder.Find("DivideBtn").GetComponent<EchoButton>();
        divideBtn.onClick.AddListener(OnClickDivideBtn);
        reduceBtn = prefabBinder.Find("ReduceBtn").GetComponent<EchoButton>();
        reduceBtn.onClick.AddListener(OnClickReduceBtn);
        addBtn = prefabBinder.Find("AddBtn").GetComponent<EchoButton>();
        addBtn.onClick.AddListener(OnClickAddBtn);
        resetBtn = prefabBinder.Find("ResetBtn").GetComponent<EchoButton>();
        resetBtn.onClick.AddListener(OnClickResetBtn);
        undoBtn = prefabBinder.Find("UndoBtn").GetComponent<EchoButton>();
        undoBtn.onClick.AddListener(OnClickUndoBtn);
        diceBtn = prefabBinder.Find("DiceBtn").GetComponent<EchoButton>();
        diceBtn.onClick.AddListener(OnClickDiceBtn);
        coinBtn = prefabBinder.Find("CoinBtn").GetComponent<EchoButton>();
        coinBtn.onClick.AddListener(OnClickCoinBtn);
        diskItem1 = prefabBinder.Find("DiskItem1").GetComponent<DiskItem>();
        diskItem2 = prefabBinder.Find("DiskItem2").GetComponent<DiskItem>();
        diskItem3 = prefabBinder.Find("DiskItem3").GetComponent<DiskItem>();
        diskItem4 = prefabBinder.Find("DiskItem4").GetComponent<DiskItem>();
        diskItem5 = prefabBinder.Find("DiskItem5").GetComponent<DiskItem>();
        diskItem6 = prefabBinder.Find("DiskItem6").GetComponent<DiskItem>();
    }

    private void Start()
    {
        EventManager.Instance.Register(EventTypeEnum.Reset, ResetLogic);
        EventManager.Instance.Register(EventTypeEnum.RefreshDisk, RefreshDisk);
        EventManager.Instance.Register(EventTypeEnum.RevocationCommandNum, RevocationCommandNum);
    }

    /// <summary>
    /// 设置数据
    /// </summary>
    /// <param name="campEnum">阵营</param>
    public void SetData(CampEnum campEnum)
    {
        camp = campEnum;
        commandLogic = new CommandLogic();
        ResetLogic(null);
        diskItem1.SetData(camp, commandLogic, 1);
        diskItem2.SetData(camp, commandLogic, 2);
        diskItem3.SetData(camp, commandLogic, 3);
        diskItem4.SetData(camp, commandLogic, 4);
        diskItem5.SetData(camp, commandLogic, 5);
        diskItem6.SetData(camp, commandLogic, 6);
    }

    /// <summary>
    /// 重置
    /// </summary>
    private void ResetLogic(EventObject eventObject)
    {
        commandLogic.Reset();
        hpValue = InitHpValue;
        hpTxt.text = hpValue.ToString();
    }

    /// <summary>
    /// 刷新生命值
    /// </summary>
    private void RefreshDisk(EventObject eventObject)
    {
        if (eventObject != null && eventObject.Param != null && eventObject.Param.Length >= 4)
        {
            CampEnum tempCamp = (CampEnum) eventObject.Param[0];
            DiskEnum tempDiskEnum = (DiskEnum) eventObject.Param[2];
            int retValue = (int) eventObject.Param[3];
            if (tempCamp == camp && tempDiskEnum == DiskEnum.HP && retValue != hpValue)
            {
                SafeShowHPValue(retValue);
            }
        }
    }

    /// <summary>
    /// 撤回时数值变化，仅变化值
    /// </summary>
    /// <param name="eventObject"></param>
    private void RevocationCommandNum(EventObject eventObject)
    {
        if (eventObject != null && eventObject.Param != null && eventObject.Param.Length >= 3)
        {
            CampEnum tempCamp = (CampEnum) eventObject.Param[0];
            DiskEnum tempDiskEnum = (DiskEnum) eventObject.Param[1];
            int retValue = (int) eventObject.Param[2];
            if (tempCamp == camp && tempDiskEnum == DiskEnum.HP)
            {
                hpValue = retValue;
            }
        }
    }

    /// <summary>
    /// 安全的显示生命值
    /// </summary>
    /// <param name="value">传入的参数</param>
    private void SafeShowHPValue(int value)
    {
        value = value < 0 ? 0 : value;
        int oldHpValue = hpValue;
        hpValue = value;
        commandLogic.ExecutiveCommand(new TextCommand(hpTxt, oldHpValue, hpValue, camp, DiskEnum.HP));
    }

    private void OnClickDivideBtn()
    {
        UIViewManager.Instance.OpenWindow(UIViewID.PayLifeView, camp, hpValue);
    }

    private void OnClickReduceBtn()
    {
        UIViewManager.Instance.OpenWindow(UIViewID.CalculatorView, camp, 0, DiskEnum.HP, hpValue, "-");
    }

    private void OnClickAddBtn()
    {
        UIViewManager.Instance.OpenWindow(UIViewID.CalculatorView, camp, 0, DiskEnum.HP, hpValue, "+");
    }

    private void OnClickResetBtn()
    {
        MessageBoxView msgBoxView = MessageBoxView.OpenWindow("Common_Cancel", msgBoxView => //取消
            {
                msgBoxView.CloseWindow();
            },
            "Common_Confirm", msgBoxView => //确定
            {
                EventManager.Instance.Execute(EventTypeEnum.Reset);
                msgBoxView.CloseWindow();
            }, null, "ResetGame"); //是否要重置游戏
        if (camp == CampEnum.Player2)
        {
            msgBoxView.transform.localScale = new Vector3(-1, -1, 1);
        }
    }

    private void OnClickUndoBtn()
    {
        commandLogic.RevocationCommand();
    }

    private void OnClickDiceBtn()
    {
        UIViewManager.Instance.OpenWindow(UIViewID.DiceSelectView, camp);
    }

    private void OnClickCoinBtn()
    {
        UIViewManager.Instance.OpenWindow(UIViewID.CoinSelectView, camp);
    }

    private void OnDestroy()
    {
        EventManager.Instance.UnRegister(EventTypeEnum.Reset, ResetLogic);
        EventManager.Instance.UnRegister(EventTypeEnum.RefreshDisk, RefreshDisk);
        EventManager.Instance.UnRegister(EventTypeEnum.RevocationCommandNum, RevocationCommandNum);
    }
}