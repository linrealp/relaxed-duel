using System;
using UnityEngine;
using EchoFramework;
using UnityEngine.UI;

/// <summary>
/// 通用弹窗界面
/// </summary>
public class MessageBoxView : UIViewBase
{
    private EchoButton bgBtn;
    private EchoText showTxt;
    private EchoButton leftBtn;
    private EchoText leftBtnTxt;
    private EchoButton rightBtn;
    private EchoText rightBtnTxt;

    /// <summary>
    /// 左按钮事件
    /// </summary>
    private Action<MessageBoxView> leftBtnAction;

    /// <summary>
    /// 右按钮事件
    /// </summary>
    private Action<MessageBoxView> rightBtnAction;

    /// <summary>
    /// 关闭事件
    /// </summary>
    private Action<MessageBoxView> closeAction;

    public override void OnAwake()
    {
        base.OnAwake();
        bgBtn = prefabBinder.Find("BgBtn").GetComponent<EchoButton>();
        bgBtn.onClick.AddListener(OnClickBgBtn);
        showTxt = prefabBinder.Find("ShowTxt").GetComponent<EchoText>();
        leftBtn = prefabBinder.Find("LeftBtn").GetComponent<EchoButton>();
        leftBtn.onClick.AddListener(OnClickLeftBtn);
        leftBtnTxt = prefabBinder.Find("LeftBtnTxt").GetComponent<EchoText>();
        rightBtn = prefabBinder.Find("RightBtn").GetComponent<EchoButton>();
        rightBtn.onClick.AddListener(OnClickRightBtn);
        rightBtnTxt = prefabBinder.Find("RightBtnTxt").GetComponent<EchoText>();
    }

    /// <summary>
    /// 设置界面数据
    /// </summary>
    /// <param name="leftLangKey">左按钮文本Key</param>
    /// <param name="leftBtnAction">左按钮事件</param>
    /// <param name="rightLangKey">右按钮文本Key</param>
    /// <param name="rightBtnAction">右按钮事件</param>
    /// <param name="descLangKey">描述文本Key</param>
    /// <param name="param">可变参数</param>
    private void SetData(string leftLangKey, Action<MessageBoxView> leftBtnAction,
        string rightLangKey, Action<MessageBoxView> rightBtnAction, Action<MessageBoxView> closeAction, string descLangKey, params object[] param)
    {
        leftBtnTxt.LoadLang(leftLangKey);
        this.leftBtnAction = leftBtnAction;
        rightBtnTxt.LoadLang(rightLangKey);
        this.rightBtnAction = rightBtnAction;
        showTxt.LoadLang(descLangKey, param);
        this.closeAction = closeAction;
    }

    /// <summary>
    /// 快捷打开通用弹窗，带两个按钮
    /// </summary>
    /// <param name="leftLangKey">左按钮文本Key</param>
    /// <param name="leftBtnAction">左按钮事件</param>
    /// <param name="rightLangKey">右按钮文本Key</param>
    /// <param name="rightBtnAction">右按钮事件</param>
    /// <param name="descLangKey">描述文本Key</param>
    /// <param name="param">可变参数</param>
    public static MessageBoxView OpenWindow(string leftLangKey, Action<MessageBoxView> leftBtnAction,
        string rightLangKey, Action<MessageBoxView> rightBtnAction, Action<MessageBoxView> closeAction, string descLangKey, params object[] param)
    {
         MessageBoxView msgBoxView = (MessageBoxView)UIViewManager.Instance.OpenWindow(UIViewID.MessageBoxView);
         msgBoxView.SetData( leftLangKey, leftBtnAction, rightLangKey, rightBtnAction, closeAction, descLangKey, param);
         return msgBoxView;
    }

    private void OnClickBgBtn()
    {
        closeAction?.Invoke(this);
        CloseWindow();
    }

    private void OnClickLeftBtn()
    {
        leftBtnAction?.Invoke(this);
    }

    private void OnClickRightBtn()
    {
        rightBtnAction?.Invoke(this);
    }
}