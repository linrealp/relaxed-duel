/****************************
 **  所有枚举均定义在此处   **
****************************/      

/// <summary>
/// 界面层级枚举
/// </summary>
public enum UIPanelTypeEnum
{
    /// <summary>
    /// 抬头显示层级，显示人物属性，血条，地图等等
    /// </summary>
    HUDPanel = 1,

    /// <summary>
    /// 对话层级
    /// </summary>
    DialogPanel = 2,

    /// <summary>
    /// 游戏界面层级，例如商店页面，物品界面等
    /// </summary>
    GamePanel = 3,

    /// <summary>
    /// 弹窗界面层级,可同时存在多个
    /// </summary>
    WindowPanel = 4,

    /// <summary>
    /// 浮窗界面层级，一些浮动的提示使用，可同时存在多个
    /// </summary>
    FloatPanel = 5,
    
    Max,
}

/// <summary>
/// 分发类型枚举
/// </summary>
public enum DispatcherEnum
{
    None = 0,
    /// <summary>
    /// 时间为单位
    /// </summary>
    Time = 1,
    /// <summary>
    /// 帧数为单位
    /// </summary>
    Frame = 2,
}

/// <summary>
/// 事件枚举
/// </summary>
public enum EventTypeEnum
{
    /// <summary>
    /// 刷新Disk
    /// </summary>
    RefreshDisk = 1,
    /// <summary>
    /// 重置
    /// </summary>
    Reset = 2,
    /// <summary>
    /// 撤回命令传输的数字
    /// </summary>
    RevocationCommandNum = 3,
}

/// <summary>
/// 界面ID枚举
/// </summary>
public enum UIViewID
{
    TestView = 1001,
    /// <summary>
    /// 计算器界面
    /// </summary>
    CalculatorView = 1002,
    /// <summary>
    /// 决斗盘界面
    /// </summary>
    DuelView = 1003,
    /// <summary>
    /// 支付生命值界面
    /// </summary>
    PayLifeView = 1004,
    /// <summary>
    /// 通用弹窗 
    /// </summary>
    MessageBoxView = 1005,
    /// <summary>
    /// 硬币选择界面
    /// </summary>
    CoinSelectView = 1006,
    /// <summary>
    /// 骰子选择界面
    /// </summary>
    DiceSelectView = 1007,
    /// <summary>
    /// 硬币动画界面
    /// </summary>
    CoinAnimView = 1008,
    /// <summary>
    /// 骰子动画界面
    /// </summary>
    DiceAnimView = 1009,
}

/// <summary>
/// 有限状态机，过渡条件枚举
/// </summary>
public enum FSMTransitionEnum
{
    Null,
}

/// <summary>
/// 有限状态机，状态枚举
/// </summary>
public enum FSMStateEnum
{
    Null,
}

/// <summary>
/// 阵营枚举
/// </summary>
public enum CampEnum
{
    None = -1,
    /// <summary>
    /// 玩家1
    /// </summary>
    Player1 = 1,
    /// <summary>
    /// 玩家2
    /// </summary>
    Player2 = 2,
}

/// <summary>
/// 盘面枚举
/// </summary>
public enum DiskEnum
{
    None = -1,
    /// <summary>
    /// 生命值
    /// </summary>
    HP = 0,
    /// <summary>
    /// 星星
    /// </summary>
    Star = 1,
    /// <summary>
    /// 三角
    /// </summary>
    Triangle = 2,
    /// <summary>
    /// 攻击力
    /// </summary>
    Attack = 3,
    /// <summary>
    /// 防御力
    /// </summary>
    Defend = 4,
    /// <summary>
    /// 魔陷
    /// </summary>
    Magic = 5,
}

/// <summary>
/// 计算类型
/// </summary>
public enum CalculateTypeEnum
{
    /// <summary>
    /// 加法
    /// </summary>
    AddOperator = 1,
    /// <summary>
    /// 减法
    /// </summary>
    ReduceOperator = 2,
    /// <summary>
    /// 乘法
    /// </summary>
    MultiOperator = 3,
    /// <summary>
    /// 除法
    /// </summary>
    DivideOperator = 4,
}

/// <summary>
/// 命令模式 空字符相关枚举
/// </summary>
public enum CommandEmptyEnum
{
    None = -1,
    /// <summary>
    /// 旧数是空
    /// </summary>
    OldNumIsEmpty = 0,
    /// <summary>
    /// 新数是空
    /// </summary>
    NewNumIsEmpty = 1,
}

/// <summary>
/// 硬币骰子枚举
/// </summary>
public enum CoinDiceEnum
{
    None = -1,
    /// <summary>
    /// 骰子
    /// </summary>
    Dice = 0,
    /// <summary>
    /// 硬币
    /// </summary>
    Coin = 11,
}