﻿/// <summary>
/// 全局工具类
/// </summary>
public class Global
{
    /// <summary>
    /// 返回语言表文本内容 
    /// </summary>
    /// <param name="langKey">关键字</param>
    /// <param name="content">传入的可变参数</param>
    /// <returns>文本内容</returns>
    public static string GetLang(string langKey, params object[] content)
    {
        return LangCfg.GetLang(langKey, content);
    }
    
    /// <summary>
    /// 返回Package语言表文本内容 
    /// </summary>
    /// <param name="langKey">关键字</param>
    /// <param name="content">传入的可变参数</param>
    /// <returns>文本内容</returns>
    public static string GetPackageLang(string langKey, params object[] content)
    {
        return LangPackageCfg.GetPackageLang(langKey, content);
    }
    
}