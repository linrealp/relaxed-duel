﻿using UnityEngine;

/// <summary>
/// 扩展方法
/// </summary>
public static class Extensions
{
    /// <summary>
    /// 清空子物体
    /// </summary>
    /// <param name="trans">自身Transform</param>
    /// <param name="isImmediately">是否立即删除</param>
    public static void ClearChildren(this Transform trans, bool isImmediately = false)
    {
        for (int i = 0; i < trans.childCount; i++)
        {
            if (isImmediately)
            {
                Object.DestroyImmediate(trans.GetChild(i).gameObject);
            }
            else
            {
                Object.Destroy(trans.GetChild(i).gameObject);
            }
        }
    }

}