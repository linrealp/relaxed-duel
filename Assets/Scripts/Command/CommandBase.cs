﻿public class CommandBase
{
    /// <summary>
    /// 执行命令
    /// </summary>
    public virtual void ExcuteCommand()
    {
    }

    /// <summary>
    /// 撤销命令
    /// </summary>
    public virtual void RevocationCommand()
    {
    }
}