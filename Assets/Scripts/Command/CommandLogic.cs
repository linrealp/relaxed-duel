﻿using System.Collections.Generic;
using EchoFramework;

/// <summary>
/// 命令模块逻辑
/// </summary>
public class CommandLogic
{
    /// <summary>
    /// 记录执行的命令
    /// </summary>
    private List<CommandBase> commandList = new List<CommandBase>();

    /// <summary>
    /// 记录前进的命令
    /// </summary>
    private List<CommandBase> precommandList = new List<CommandBase>();
    
    /// <summary>
    /// 执行命令
    /// </summary>
    /// <param name="command">需要执行的命令</param>
    public void ExecutiveCommand(CommandBase command)
    {
        precommandList.Clear();
        commandList.Add(command);
        command.ExcuteCommand();
    }

    /// <summary>
    /// 撤回命令
    /// </summary>
    public void RevocationCommand()
    {
        if (commandList.Count > 0)
        {
            CommandBase command = commandList[commandList.Count - 1];
            commandList.Remove(command);
            precommandList.Add(command);
            command.RevocationCommand();
        }
    }

    /// <summary>
    /// 向前执行命令
    /// </summary>
    public void ForwardExcuteCommand()
    {
        if (precommandList.Count > 0)
        {
            CommandBase command = precommandList[precommandList.Count - 1];
            precommandList.Remove(command);
            commandList.Add(command);
            command.ExcuteCommand();
        }
    }

    /// <summary>
    /// 重置
    /// </summary>
    public void Reset()
    {
        commandList.Clear();
        precommandList.Clear();
    }
}