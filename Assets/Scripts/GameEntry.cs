﻿using System;
using EchoFramework;
using UnityEngine;

/// <summary>
/// 游戏入口
/// </summary>
public class GameEntry : Singleton<GameEntry>
{
    /// <summary>
    /// 是否启用AB包模式
    /// </summary>
    [Header("AB包模式")]
    public bool AssetBundleMode;

    private void Start()
    {
#if !UNITY_EDITOR
        AssetBundleMode = true;
#endif
        DontDestroyOnLoad(gameObject);

        GameLogger.Instance.OnInit();
        TimeManager.Instance.OnInit();
        EventManager.Instance.OnInit();
        AssetManager.Instance.OnInit();
        LogicModuleManager.Instance.OnInit();
        UIViewManager.Instance.OnInit();

        GameLogger.LogYellow("是否启用AB包模式：" + AssetBundleMode);
        UIViewManager.Instance.OpenWindow(UIViewID.DuelView);
    }

    private void Update()
    {
        GameLogger.Instance.OnUpdate();
        EventManager.Instance.OnUpdate();
        TimeManager.Instance.OnUpdate();
        AssetManager.Instance.OnUpdate();
        LogicModuleManager.Instance.OnUpdate();
        UIViewManager.Instance.OnUpdate();
    }

    private void OnDestroy()
    {
        GameLogger.Instance.OnClose();
        EventManager.Instance.OnClose();
        TimeManager.Instance.OnClose();
        AssetManager.Instance.OnClose();
        LogicModuleManager.Instance.OnClose();
        UIViewManager.Instance.OnClose();
    }
}