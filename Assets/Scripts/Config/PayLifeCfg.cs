﻿using EchoFramework;

public class PayLifeCfg : Config
{
    /// <summary>
    /// 描述
    /// </summary>
    [ConfigMark]
    public readonly string Desc;

    /// <summary>
    /// 计算类型
    /// </summary>
    [ConfigMark]
    public readonly int CalculateType;

    /// <summary>
    /// 计算数
    /// </summary>
    [ConfigMark]
    public readonly int CalculateNum;
}