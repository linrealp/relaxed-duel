using System.Collections;
using System.Collections.Generic;
using EchoFramework;
using UnityEngine;

/// <summary>
/// 语言配置表
/// </summary>
public class LangCfg : Config
{
    /// <summary>
    /// 关键字
    /// </summary>
    [ConfigMark]
    public readonly string LangKey;

    /// <summary>
    /// 文本内容
    /// </summary>
    [ConfigMark]
    public readonly string Content;

    /// <summary>
    /// 语言表字典
    /// key:语言表关键字
    /// Value:语言表内容
    /// </summary>
    private static Dictionary<string, string> langKey2ContentDict;

    /// <summary>
    /// 返回语言表文本内容 <br/>
    /// 请使用<see cref="Global.GetLang"/>
    /// </summary>
    /// <param name="langKey">关键字</param>
    /// <param name="content">传入参数</param>
    /// <returns>文本内容</returns>
    public static string GetLang(string langKey, params object[] content)
    {
        if (string.IsNullOrEmpty(langKey))
            return "New EchoText";
        
        if (langKey2ContentDict == null)
        {
            langKey2ContentDict = new Dictionary<string, string>();
            Dictionary<int, LangCfg> langDict = ConfigManager<LangCfg>.Instance.GetData();
            foreach (LangCfg langCfgItem in langDict.Values)
            {
                if (langKey2ContentDict.ContainsKey(langCfgItem.LangKey))
                {
                    GameLogger.LogError("语言表已存在关键字：" + langCfgItem.LangKey);
                    continue;
                }

                langKey2ContentDict.Add(langCfgItem.LangKey, langCfgItem.Content);
            }
        }

        if (langKey2ContentDict.ContainsKey(langKey))
        {
            return string.Format(langKey2ContentDict[langKey], content);
        }
        else
        {
            return string.Format(langKey, content);
        }
    }
}