﻿using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

namespace EchoFramework.Editor
{
    public class EchoImageMenu
    {
        [MenuItem("EchoFramework/UI/EchoImage", false)]
        [MenuItem("GameObject/UI/EchoImage", false)]
        public static GameObject CreateEchoImage()
        {
            GameObject go = new GameObject("New EchoImage");
            RectTransform rectTrans = go.AddComponent<RectTransform>();
            rectTrans.sizeDelta = new Vector2(100f, 100f);
            if (Selection.activeObject != null && Selection.activeObject is GameObject)
            {
                Transform parentTrans = ((GameObject) Selection.activeObject).transform;
                go.transform.SetParent(parentTrans, false);
            }

            go.AddComponent<EchoImage>();
            Selection.activeObject = go;

            return go;
        }
    }
}