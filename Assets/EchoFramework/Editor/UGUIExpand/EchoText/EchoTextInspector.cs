using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

namespace EchoFramework.Editor
{
    /// <summary>
    /// <see cref="EchoText"/>的编辑器面板
    /// </summary>
    [CustomEditor(typeof(EchoText), false)]
    public class EchoTextInspector : UnityEditor.Editor
    {
        /// <summary>
        /// 面板参数
        /// </summary>
        private SerializedProperty langKeySp;
        /// <summary>
        /// 自身EText
        /// </summary>
        private EchoText selfTxt;

        private void OnEnable()
        {
            selfTxt = target as EchoText;
            langKeySp = serializedObject.FindProperty("langKey");
        }

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            string langKey = langKeySp.stringValue;
            if (GUILayout.Button("加载文本"))
            {
                if (langKey.StartsWith("#"))
                {
                    selfTxt.text = Global.GetPackageLang(langKey);
                }
                else
                {
                    selfTxt.text = Global.GetLang(langKey);
                }
            }
        }
    }
}