﻿using UnityEditor;
using UnityEngine;

namespace EchoFramework.Editor
{
    public class EchoTextMenu
    {
        [MenuItem("EchoFramework/UI/EchoText", false)]
        [MenuItem("GameObject/UI/EchoText", false)]
        public static GameObject CreateEchoText()
        {
            GameObject go = new GameObject("New EchoText");
            RectTransform rectTrans = go.AddComponent<RectTransform>();
            rectTrans.localScale = new Vector3(.5f, .5f, .5f);
            rectTrans.sizeDelta = new Vector2(350f, 75f);
            if (Selection.activeObject != null && Selection.activeObject is GameObject)
            {
                Transform parentTrans = ((GameObject) Selection.activeObject).transform;
                go.transform.SetParent(parentTrans, false);
            }
            EchoText echoTxt = go.AddComponent<EchoText>();
            echoTxt.fontSize = 35;
            echoTxt.color = Color.white;
            echoTxt.alignment = TextAnchor.MiddleCenter;
            echoTxt.text = "New EchoText";
            Selection.activeObject = go;

            return go;
        }
    }
}