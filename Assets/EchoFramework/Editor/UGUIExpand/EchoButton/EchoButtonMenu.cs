﻿using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

namespace EchoFramework.Editor
{
    public class EchoButtonMenu
    {
        [MenuItem("EchoFramework/UI/EchoButton", false)]
        [MenuItem("GameObject/UI/EchoButton", false)]
        public static GameObject CreateEchoButton()
        {
            GameObject go = new GameObject("New EchoButton");
            RectTransform rectTrans = go.AddComponent<RectTransform>();
            rectTrans.sizeDelta = new Vector2(160f, 30f);
            if (Selection.activeObject != null && Selection.activeObject is GameObject)
            {
                Transform parentTrans = ((GameObject) Selection.activeObject).transform;
                go.transform.SetParent(parentTrans, false);
            }
            EchoImage echoImg = go.AddComponent<EchoImage>();
            echoImg.sprite = AssetDatabase.GetBuiltinExtraResource<Sprite>("UI/Skin/UISprite.psd");
            echoImg.type = Image.Type.Sliced;
            go.AddComponent<EchoButton>();
            GameObject textGo = EchoTextMenu.CreateEchoText();
            textGo.name = "Text";
            EchoText echoTxt = textGo.GetComponent<EchoText>();
            echoTxt.text = "EchoButton";
            echoTxt.color = Color.black;
            textGo.transform.SetParent(go.transform);
            Selection.activeObject = go;
            return go;
        }
    }
}