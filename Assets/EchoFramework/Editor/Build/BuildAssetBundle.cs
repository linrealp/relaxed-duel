using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using UnityEditor;
using UnityEngine;
using LitJson;

namespace EchoFramework.Editor
{
    /// <summary>
    /// AB包打包工具
    /// </summary>
    public class BuildAssetBundle
    {
        /// <summary>
        /// 需要打包的资源所在路径
        /// </summary>
        private static string rootPath = Application.dataPath + "/GameRes";

        /// <summary>
        /// AB包输出路径
        /// </summary>
        private static string outABPath = Application.streamingAssetsPath;

        /// <summary>
        /// 记录每个资源对应的AB包
        /// key:资源路径 例如："Assets/GameRes/UIPrefab/Sphere.prefab"
        /// value:对应的AB包 例如："assets_gameres_uiprefab"
        /// </summary>
        private static Dictionary<string, string> asset2BundleDict = new Dictionary<string, string>();

        /// <summary>
        /// 记录需要打包的AssetBundleBuild
        /// </summary>
        private static List<AssetBundleBuild> assetBundleBuildList = new List<AssetBundleBuild>();

        /// <summary>
        /// 记录每个资源依赖的AB包列表
        /// key:资源路径 例如："Assets/GameRes/UIPrefab/Sphere.prefab"
        /// value:依赖的AB包列表
        /// </summary>
        private static Dictionary<string, List<string>> asset2DependenciesDict = new Dictionary<string, List<string>>();

        /// <summary>
        /// 打包生成所有的AB包
        /// </summary>
        [MenuItem("EchoFramework/Build/BuildAllAssetBundle")]
        public static void BuildAllAB()
        {
            GameLogger.Log("开始打AB包...");
            if (Directory.Exists(outABPath))
            {
                Directory.Delete(outABPath, true);
            }

            Directory.CreateDirectory(outABPath);
            AssetDatabase.Refresh();

            asset2BundleDict.Clear();
            assetBundleBuildList.Clear();
            asset2DependenciesDict.Clear();
            //GameRes文件夹
            DirectoryInfo rootDir = new DirectoryInfo(rootPath);
            //获取资源路径下的所有文件夹
            //注意：GameRes当前目录不可直接放置资源，会打包不到!!!
            DirectoryInfo[] sonDirs = rootDir.GetDirectories();
            if (sonDirs.Length <= 0)
            {
                GameLogger.LogWarning("GameRes文件夹为空!!!");
                return;
            }

            foreach (DirectoryInfo dir in sonDirs)
            {
                ScanDirectory(dir);
            }

            GameLogger.Log("开始生成资源Json文件...");
            //计算依赖
            CalAssetDependencies();
            //生成Json资源文件
            GenerateAssetJson();
            GameLogger.Log("生成资源Json文件完成!!!");

            //打AB包
            BuildPipeline.BuildAssetBundles(outABPath, assetBundleBuildList.ToArray(), BuildAssetBundleOptions.None,
                EditorUserBuildSettings.activeBuildTarget);
            GameLogger.Log("AB包打包完成!!!");

            AssetDatabase.Refresh();
        }


        /// <summary>
        /// 扫描文件夹 处理为AssetBundleBuild
        /// </summary>
        /// <param name="dirInfo">目标文件夹</param>
        private static void ScanDirectory(DirectoryInfo dirInfo)
        {
            List<string> assetNameList = new List<string>();
            FileInfo[] fileInfos = dirInfo.GetFiles();
            foreach (FileInfo fileInfo in fileInfos)
            {
                if (fileInfo.Name.EndsWith(".meta"))
                    continue;

                //assetName的格式类似 "Assets/GameRes/UIPrefab/Sphere.prefab"
                string assetName = fileInfo.FullName.Substring(Application.dataPath.Length - "Assets".Length)
                    .Replace('\\', '/');
                assetNameList.Add(assetName);
            }

            if (assetNameList.Count > 0)
            {
                AssetBundleBuild assetBundleBuild = new AssetBundleBuild();
                //assetBundleName的格式类似 "assets_gameres_uiprefab"
                string assetBundleName = dirInfo.FullName.Substring(Application.dataPath.Length - "Asset".Length - 1)
                    .Replace("\\", "_").ToLower();
                assetBundleBuild.assetBundleName = assetBundleName;
                assetBundleBuild.assetNames = new string[assetNameList.Count];
                for (int i = 0; i < assetNameList.Count; i++)
                {
                    assetBundleBuild.assetNames[i] = assetNameList[i];
                    asset2BundleDict.Add(assetNameList[i], assetBundleName);
                }

                assetBundleBuildList.Add(assetBundleBuild);
                GameLogger.Log($"AB包：{assetBundleName} 扫描完成");
            }

            //继续扫描下一级文件夹
            DirectoryInfo[] sonDirInfos = dirInfo.GetDirectories();
            if (sonDirInfos.Length > 0)
            {
                foreach (DirectoryInfo sonDirInfo in sonDirInfos)
                {
                    ScanDirectory(sonDirInfo);
                }
            }
        }

        /// <summary>
        /// 计算Asset依赖
        /// </summary>
        private static void CalAssetDependencies()
        {
            foreach (string asset in asset2BundleDict.Keys)
            {
                //当前资源对应的AB包
                string assetBundle = asset2BundleDict[asset];
                //获取当前资源对应的依赖
                string[] dependencies = AssetDatabase.GetDependencies(asset);
                //获取依赖的资源列表
                List<string> dependentAssetList = new List<string>();
                if (dependencies != null && dependencies.Length > 0)
                {
                    foreach (string dependentAsset in dependencies)
                    {
                        if (dependentAsset == asset || dependentAsset.EndsWith(".cs"))
                            continue;
                        dependentAssetList.Add(dependentAsset);
                    }
                }

                //依赖的AB包列表
                List<string> dependentABList = new List<string>();
                if (dependentAssetList.Count > 0)
                {
                    foreach (string dependentAsset in dependentAssetList)
                    {
                        bool result = asset2BundleDict.TryGetValue(dependentAsset, out string dependentAssetBundle);
                        if (result)
                        {
                            if (dependentAssetBundle != assetBundle && !dependentABList.Contains(dependentAssetBundle))
                            {
                                dependentABList.Add(dependentAssetBundle);
                            }
                        }
                    }
                }

                //存入依赖字典
                asset2DependenciesDict.Add(asset, dependentABList);
            }
        }

        /// <summary>
        /// 生成资源Json文件
        /// </summary>
        private static void GenerateAssetJson()
        {
            AssetRecordConfig assetRecordConfig = new AssetRecordConfig();

            //AB包信息
            foreach (AssetBundleBuild assetBundleBuild in assetBundleBuildList)
            {
                AssetBundleInfo assetBundleInfo = new AssetBundleInfo();
                assetBundleInfo.AssetBundleName = assetBundleBuild.assetBundleName;
                foreach (string assetName in assetBundleBuild.assetNames)
                {
                    assetBundleInfo.AssetList.Add(assetName);
                }

                assetRecordConfig.AssetBundleInfoList.Add(assetBundleInfo);
            }

            //Asset资源信息
            foreach (KeyValuePair<string, string> asset2Bundle in asset2BundleDict)
            {
                AssetInfo assetInfo = new AssetInfo();
                assetInfo.AssetPath = asset2Bundle.Key;
                assetInfo.AssetBundleName = asset2Bundle.Value;
                bool result = asset2DependenciesDict.TryGetValue(asset2Bundle.Key, out List<string> dependentABList);
                if (result)
                {
                    if (dependentABList.Count > 0)
                    {
                        foreach (string dependentAssetBundle in dependentABList)
                        {
                            assetInfo.DependentAssetBundleList.Add(dependentAssetBundle);
                        }
                    }
                }

                assetRecordConfig.AssetInfoList.Add(assetInfo);
            }

            //写入Json
            JsonWriter jw = new JsonWriter();
            jw.PrettyPrint = true;
            JsonMapper.ToJson(assetRecordConfig, jw);
            string jsonStr = jw.ToString();
            Regex reg = new Regex(@"(?i)\\[uU]([0-9a-f]{4})");
            jsonStr = reg.Replace(jsonStr, delegate (Match m) { return ((char)Convert.ToInt32(m.Groups[1].Value, 16)).ToString(); });
            StreamWriter sw = new StreamWriter(Path.Combine(outABPath, "AssetRecordConfig.json"));
            sw.Write(jsonStr);
            sw.Flush();
            sw.Close();
        }
    }
}