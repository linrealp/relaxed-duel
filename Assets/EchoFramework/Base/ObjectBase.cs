﻿using UnityEngine;

namespace EchoFramework
{
    /// <summary>
    /// 框架基类，只要是游戏物体，都需继承该类
    /// </summary>
    [RequireComponent(typeof(PrefabBinder))]
    public class ObjectBase : MonoBehaviour
    {
        
    }
}