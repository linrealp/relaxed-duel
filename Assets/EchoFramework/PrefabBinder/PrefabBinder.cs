﻿using System.Collections.Generic;
using UnityEngine;
using System;
using Unity.VisualScripting;
using Object = UnityEngine.Object;

namespace EchoFramework
{
    /// <summary>
    /// 预设物体绑定器
    /// </summary>
    public class PrefabBinder : MonoBehaviour
    {
        /// <summary>
        /// 绑定器Item
        /// </summary>
        [Serializable]
        public class Item
        {
            public string name;
            public Object obj;
        }

        /// <summary>
        /// 绑定器Item数组
        /// </summary>
        public Item[] items = new Item[0];

        /// <summary>
        /// 绑定器所有物体字典
        /// key: 名称
        /// value: Unity物体
        /// </summary>
        private Dictionary<string, Object> itemDict = new Dictionary<string, Object>();

        private void Awake()
        {
            Init(); //初始化物体字典
        }

        /// <summary>
        /// 初始化物体字典
        /// </summary>
        private void Init()
        {
            for (int i = 0; i < items.Length; i++)
            {
                itemDict.Add(items[i].name, items[i].obj);
            }
        }

        /// <summary>
        /// 找寻指定名称的Transform
        /// </summary>
        /// <param name="objName">名称</param>
        /// <returns>返回Transform</returns>
        public Transform Find(string objName)
        {
            if (string.IsNullOrEmpty(objName))
                return null;
            if (itemDict.ContainsKey(objName))
            {
                Object obj = itemDict[objName];
                Component com =  obj as Component;
                return com.transform;
            }
            
            return null;
        }
    }
}