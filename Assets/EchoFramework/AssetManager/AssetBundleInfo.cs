﻿using System.Collections.Generic;

namespace EchoFramework
{
    /// <summary>
    /// AB包信息 用于序列化为Json文件
    /// </summary>
    public class AssetBundleInfo
    {
        /// <summary>
        /// AB包名称 例如："assets_gameres_uiprefab"
        /// </summary>
        public string AssetBundleName { get; set; }
        
        /// <summary>
        /// AB对应的所有资源路径 单个资源路径例如："Assets/GameRes/UIPrefab/Sphere.prefab"
        /// </summary>
        public List<string> AssetList { get; set; }

        public AssetBundleInfo()
        {
            AssetList = new List<string>();
        }
    }
}