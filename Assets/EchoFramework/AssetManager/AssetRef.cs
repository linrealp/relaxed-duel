﻿using System.Collections.Generic;
using UnityEngine;

namespace EchoFramework
{
    /// <summary>
    /// 内存中的AsserRef对象
    /// </summary>
    public class AssetRef
    {
        /// <summary>
        /// 当前Asset资源的配置信息
        /// </summary>
        public AssetInfo AssetInfo { get; set; }

        /// <summary>
        /// 当前Asset资源所属的AssetBundleRef对象
        /// </summary>
        public AssetBundleRef AssetBundleRef { get; set; }

        /// <summary>
        /// 当前Asset资源依赖的AssetBundleRef对象列表
        /// </summary>
        public List<AssetBundleRef> DependentAssetBundleRefList { get; set; }

        /// <summary>
        /// 从AssetBundle文件中提取出来的资源对象
        /// </summary>
        public Object Asset { get; set; }

        /// <summary>
        /// 是否是Prefab
        /// </summary>
        public bool IsGameObject { get; set; }

        /// <summary>
        /// 当前资源被哪些GameObject所依赖
        /// </summary>
        public List<GameObject> ChildrenList { get; set; }
    }
}