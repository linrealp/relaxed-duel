﻿using System.Collections.Generic;

namespace EchoFramework
{
    /// <summary>
    /// Asset资源信息 用于序列化为Json文件
    /// </summary>
    public class AssetInfo
    {
        /// <summary>
        /// Asset资源路径 例如："Assets/GameRes/UIPrefab/Sphere.prefab"
        /// </summary>
        public string AssetPath { set; get; }
        
        /// <summary>
        /// AB包名称 例如："assets_gameres_uiprefab"
        /// </summary>
        public string AssetBundleName { set; get; }
        
        /// <summary>
        /// 所依赖的AB包名称列表  单个AB名称例如："assets_gameres_uiprefab"
        /// </summary>
        public List<string> DependentAssetBundleList { set; get; }

        public AssetInfo()
        {
            DependentAssetBundleList = new List<string>();
        }
    }
}