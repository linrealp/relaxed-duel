﻿using UnityEngine;
using System.Collections.Generic;

namespace EchoFramework
{
    /// <summary>
    /// 内存中的AssetBundleRef对象
    /// </summary>
    public class AssetBundleRef
    {
        /// <summary>
        /// 当前AB包的配置信息
        /// </summary>
        public AssetBundleInfo AssetBundleInfo { get; set; }
        
        /// <summary>
        /// 加载到内存的AB包对象
        /// </summary>
        public AssetBundle AssetBundle { get; set; }
        
        /// <summary>
        /// 当前AssetBundleRef被哪些资源AssetRef所依赖
        /// </summary>
        public List<AssetRef> ChildrenAssetRefList { get; set; }

    }
}