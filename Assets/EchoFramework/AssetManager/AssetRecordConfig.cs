﻿using System.Collections.Generic;

namespace EchoFramework
{
    /// <summary>
    /// 游戏整个AB包资源信息  用于序列化为Json文件
    /// </summary>
    public class AssetRecordConfig
    {
        /// <summary>
        /// 游戏AB包信息List
        /// </summary>
        public List<AssetBundleInfo> AssetBundleInfoList { get; set; }
        
        /// <summary>
        /// 游戏Asset资源信息List
        /// </summary>
        public List<AssetInfo> AssetInfoList { get; set; }

        public AssetRecordConfig()
        {
            AssetBundleInfoList = new List<AssetBundleInfo>();
            AssetInfoList = new List<AssetInfo>();
        }
    }
}