﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EchoFramework
{
    /// <summary>
    /// 配置表基类  只有用[ConfigMark]标记且readonly声明的字段才会被识别为需要反射的配置数据
    /// </summary>
    public class Config
    {
        /// <summary>
        /// ID
        /// </summary>
        [ConfigMark]
        public readonly int ID;

    }

    /// <summary>
    /// 标记配置表字段使用的特性
    /// </summary>
    public class ConfigMarkAttribute : Attribute
    {
    }
}
