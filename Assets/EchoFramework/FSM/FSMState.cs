﻿using System.Collections.Generic;
using UnityEngine;

namespace EchoFramework
{
    /// <summary>
    /// 有限状态机状态 抽象类
    /// </summary>
    public abstract class FSMState
    {
        /// <summary>
        /// 有限状态机管理器
        /// </summary>
        protected FSMSystem fsmSystem;
        /// <summary>
        /// 属于的对象
        /// </summary>
        protected GameObject owner;
        /// <summary>
        /// 状态转换字典
        /// key:过渡条件
        /// value:状态枚举
        /// </summary>
        protected Dictionary<FSMTransitionEnum, FSMStateEnum> trans2stateEnumDict;
        /// <summary>
        /// 自身状态枚举
        /// </summary>
        public FSMStateEnum stateEnum;
        /// <summary>
        /// 进度状态时调用
        /// </summary>
        public abstract void OnEnter();
        /// <summary>
        /// 状态持续时调用(当前状态执行的逻辑放在这里)
        /// </summary>
        public abstract void OnUpdate();
        /// <summary>
        /// 状态持续时调用(状态转换条件放在这里)
        /// </summary>
        public abstract void OnReason();
        /// <summary>
        /// 退出状态时调用
        /// </summary>
        public abstract void OnExit();


        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="fsmSystem">有限状态机管理器</param>
        /// <param name="state">自身状态</param>
        /// <param name="owner">当前状态属于的对象</param>
        public FSMState(FSMSystem fsmSystem, FSMStateEnum state, GameObject owner)
        {
            this.fsmSystem = fsmSystem;
            this.stateEnum = state;
            this.owner = owner;
            trans2stateEnumDict = new Dictionary<FSMTransitionEnum, FSMStateEnum>();
        }

        /// <summary>
        /// 添加过渡条件
        /// </summary>
        /// <param name="transition">过渡条件</param>
        /// <param name="nextState">下一个状态</param>
        public void AddTransition(FSMTransitionEnum transition, FSMStateEnum nextState)
        {
            if (transition == FSMTransitionEnum.Null || trans2stateEnumDict.ContainsKey(transition))
            {
                GameLogger.LogWarning(string.Format("{0} 添加的过渡条件为Null或已存在该过渡条件", owner.name), owner);
                return;
            }
            trans2stateEnumDict.Add(transition, nextState);
        }

        /// <summary>
        /// 删除过渡条件
        /// </summary>
        /// <param name="transition">过渡条件</param>
        public void DeleteTransition(FSMTransitionEnum transition)
        {
            if (transition == FSMTransitionEnum.Null || !trans2stateEnumDict.ContainsKey(transition))
            {
                GameLogger.LogWarning(string.Format("{0} 删除的过渡条件为Null或该过渡条件不存在", owner.name), owner);
            }
        }

        /// <summary>
        /// 获取过渡条件指向的下一个状态
        /// </summary>
        /// <param name="transition">过渡条件</param>
        /// <returns>下一个状态</returns>
        public FSMStateEnum GetNextState(FSMTransitionEnum transition)
        {
            if (transition == FSMTransitionEnum.Null)
            {
                GameLogger.LogError(string.Format("{0} 传入的过渡条件为Null！", owner.name), owner);
                return FSMStateEnum.Null;
            }
            if (!trans2stateEnumDict.ContainsKey(transition))
            {
                GameLogger.LogError(string.Format("{0} 该状态不存在过渡条件 {1}", owner.name, transition), owner);
                return FSMStateEnum.Null;
            }
            return trans2stateEnumDict[transition];
        }
    }
}
