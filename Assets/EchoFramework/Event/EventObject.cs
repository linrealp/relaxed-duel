﻿using System;
using System.Collections.Generic;

namespace EchoFramework
{
    /// <summary>
    /// 事件中心辅助传参类
    /// </summary>
    public class EventObject
    {
        private object[] param;
        public object[] Param
        {
            set { param = value; }
            get { return param; }
        }

        public EventObject(params object[] param)
        {
            Param = param;
        }
    }
}
