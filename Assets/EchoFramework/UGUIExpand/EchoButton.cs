﻿    using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using UnityEngine.Events;
using UnityEngine.UI;
using UnityEngine.Serialization;
using UnityEngine;

namespace EchoFramework
{
    /// <summary>
    /// 自己重写的 Button 按钮
    /// 1、单击
    /// 2、双击
    /// 3、长按
    /// </summary>    
    public class EchoButton : Selectable
    {
        [Header("开启单击")]
        public bool enableSingleClick = true;
        /// <summary>
        /// 单击事件
        /// </summary>
        public UnityEvent onClick;

        [Header("开启双击")]
        public bool enableDoubleClick;

        /// <summary>
        /// 双击事件
        /// </summary>
        public UnityEvent onDoubleClick;

        [Header("开启长按")]
        public bool enableLongPress;

        /// <summary>
        /// 长按事件
        /// </summary>
        public UnityEvent onLongPress;

        /// <summary>
        /// 双击判定时间
        /// </summary>
        private float doubleClickJudgeTime = 0.3f;

        /// <summary>
        /// 长按判定时间
        /// </summary>
        private float pressJudgeTime = 0.6f;

        /// <summary>
        /// 是否按下
        /// </summary>
        private bool isPointerDown;

        /// <summary>
        /// 是否触发长按
        /// </summary>
        private bool isTriggerLongPress;

        /// <summary>
        /// 按下时间
        /// </summary>
        private float pointerDownTime;

        /// <summary>
        /// 点击的间隔时间
        /// </summary>
        private float clickIntervalTime;

        /// <summary>
        /// 点击次数
        /// </summary>
        private float clickTimes;

        private void Update()
        {
            if (isPointerDown)
            {
                if (isTriggerLongPress) return; //已经触发了长按
                pointerDownTime += Time.deltaTime;
                if (pointerDownTime >= pressJudgeTime)
                {
                    if (enableLongPress) onLongPress?.Invoke();
                    isTriggerLongPress = true;
                    pointerDownTime = 0;
                }
            }

            if (clickTimes >= 1)
            {
                clickIntervalTime += Time.deltaTime;
                //双击需要有一个判定时间，而判定时间会导致单击触发有延迟，为了减少对单击的影响，在不开启双击操作的时候，绕过判定时间
                if (enableDoubleClick) 
                {
                    if (clickIntervalTime >= doubleClickJudgeTime)
                    {
                        if (clickTimes >= 2)
                        {
                            if (enableDoubleClick) onDoubleClick?.Invoke();
                        }
                        else
                        {
                            if (enableSingleClick) onClick?.Invoke();
                        }

                        DoStateTransition(SelectionState.Highlighted, false);
                        clickTimes = 0;
                        clickIntervalTime = 0;
                    }
                }
                else
                {
                    if (enableSingleClick) onClick?.Invoke();
                    DoStateTransition(SelectionState.Highlighted, false);
                    clickTimes = 0;
                    clickIntervalTime = 0;
                }
                
                
            }
        }

        public override void OnPointerDown(PointerEventData eventData)
        {
            base.OnPointerDown(eventData);
            isPointerDown = true;
        }

        public override void OnPointerUp(PointerEventData eventData)
        {
            base.OnPointerUp(eventData);
            isPointerDown = false;
            if (!isTriggerLongPress)
                clickTimes++;
            isTriggerLongPress = false;
            pointerDownTime = 0;
        }

        public override void OnPointerEnter(PointerEventData eventData)
        {
            base.OnPointerEnter(eventData);
            DoStateTransition(SelectionState.Highlighted, true);
        }

        public override void OnPointerExit(PointerEventData eventData)
        {
            base.OnPointerExit(eventData);
            DoStateTransition(SelectionState.Normal, true);
        }
    }
}