﻿using UnityEngine;
using UnityEngine.UI;

namespace EchoFramework
{
    /// <summary>
    /// 图片组件
    /// </summary>
    public class EchoImage : Image
    {
        /// <summary>
        /// 设置图片
        /// </summary>
        /// <param name="imgPath"></param>
        public void SetImg(string imgPath)
        {
            Sprite imgSprite = AssetManager.Instance.LoadAsset<Sprite>(imgPath, gameObject);
            if (imgSprite != null)
            {
                sprite = imgSprite;
            }
        }
    }
}