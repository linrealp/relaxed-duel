using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace EchoFramework
{
    /// <summary>
    /// 文本组件
    /// </summary>
    public class EchoText : Text
    {
        [Header("语言表关键字")]
        [SerializeField]
        private string langKey;

        /// <summary>
        /// 语言参数
        /// </summary>
        private object[] paras;

        /// <summary>
        /// 加载语言
        /// </summary>
        /// <param name="langKey">语言表关键词</param>
        /// <param name="paras">可变参数</param>
        public void LoadLang(string langKey, params object[] paras)
        {
            this.langKey = langKey;
            this.paras = paras;
            if (langKey.StartsWith("#"))
            {
                text = Global.GetPackageLang(this.langKey, this.paras);
            }
            else
            {
                text = Global.GetLang(this.langKey, this.paras);
            }
        }
    }
}