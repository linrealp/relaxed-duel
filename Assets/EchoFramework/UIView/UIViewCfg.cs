using System.Collections.Generic;

namespace EchoFramework
{
    /// <summary>
    /// UIView配置类
    /// </summary>
    public class UIViewCfg : Config
    {
        /// <summary>
        /// 界面ID
        /// </summary>
        [ConfigMark]
        public readonly int UIViewID;
        /// <summary>
        /// 界面层级类型
        /// </summary>
        [ConfigMark]
        public readonly int UIPanelType;
        /// <summary>
        /// 资源路径
        /// </summary>
        [ConfigMark]
        public readonly string ResPath;

        /// <summary>
        /// 界面ID
        /// </summary>
        public UIViewID ViewID { get => (UIViewID)UIViewID; }
        
        /// <summary>
        /// 界面层级
        /// </summary>
        public UIPanelTypeEnum PanelTypeEnum { get => (UIPanelTypeEnum)UIPanelType; }

        /// <summary>
        /// 通过界面ID获取界面配置
        /// </summary>
        /// <param name="viewID">界面ID</param>
        public static UIViewCfg GetUIViewConfigByViewID(int viewID)
        {
            List<UIViewCfg> viewCfgList = ConfigManager<UIViewCfg>.Instance.GetDataList();
            for (int i = 0; i < viewCfgList.Count; i++)
            {
                if (viewCfgList[i].UIViewID == viewID)
                    return viewCfgList[i];
            }
            return null;
        }
    }
}