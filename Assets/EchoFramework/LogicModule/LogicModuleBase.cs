﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EchoFramework
{
    /// <summary>
    /// 模块功能逻辑单例基类
    /// </summary>
    public class LogicModuleBase
    {
        /// <summary>
        /// 初始化
        /// </summary>
        public virtual void OnInit()
        {

        }

        /// <summary>
        /// 每帧更新
        /// </summary>
        public virtual void OnUpdate()
        {

        }

        /// <summary>
        /// 关闭时调用
        /// </summary>
        public virtual void OnClose()
        {

        }
    }
}
